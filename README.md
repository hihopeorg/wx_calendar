# wx_calendar
## 简介
>该组件是一个对日历的组件封装，主要功能有翻转日历，跳转指定日期，获取当前已选，取消选中日期，设置代办事项，删除指定代办事项，清空代办事项，获取所有代办，禁选指定日期，指定可选区域，指定特定可选，选中指定日期，周月视图的切换，获取自定义的配置，获取日历面板详情等功能。


#### 效果展示
![avatar](./calendar.gif)

## 下载安装
```shell
npm install @ohos/wx_calendar --save
```
OpenHarmony npm环境配置等更多内容，请参考 [如何安装OpenHarmony npm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_npm_usage.md) 。

#### 安装教程

方法1.

1. 下载wx_calendar源码。
2. 拷贝wx_calendar源码中default目录文件到component和template目录下。
3. 在demo中引用拷贝的文件中的方法。
#### 使用说明

######    1.1 页面展示日历

######    hml中实现：

   ```
<element name="am-calendar" src="../../component/v2/index.hml"></element>
<div style="flex-direction: column;">
    <div style="width: 90%;height:400px;overflow:hidden;">
        <am-calendar id="calendar" config="{{calendarConfig}}"></am-calendar>
    </div>
</div>
   ```
###### 		js中实现：

   ```
		export default{
			data: {
				calendarConfig: {
					theme: 'elegant'
				},
			},
		}
   ```
## 接口说明
1. 跳转指定日期-jump
2. 获取当前已选-getSelectedDates
3. 取消选中日期-cancelSelectedDates
4. 设置代办事项-setTodos
5. 删除指定代办事项-deleteTodos
6. 清空代办事项-clearTodos
7. 获取所有代办-getTodos
8. 禁选指定日期-disableDates
9. 指定可选区域-enableArea
10. 选中指定日期-setSelectedDates
11. 指定特定可选-enableDates 
12. 周月视图切换-switchView
13. 获取自定义配置-getConfig
14. 获取日历面板日期-getCalendarDates

## 兼容性
支持 OpenHarmony API version 8 及以上版本。

## 目录结构
````
|---- wx_calendar 
|     |---- entry  # 示例代码文件夹
            |---- src
                |----js
                    |----MainAbility
                        |----pages
                            |----index
                                |----index.css          #demo的样式
                                |----index.hml          #demo    
                                |----index.js           #demo的js
        |----template
            |----src
                |----main
                    |----js
                        |----components
                            |----calendar
                                |----index.css  #日历js
                                |----index.hml  #日历页面
                                |----index.js   #日历js 
                                |----sxSata.js  #数据处理
                            |----datepicker
                                |----index.css  #时间选择css
                                |----index.hml  #时间选择页面
                                |----index.js   #时间处理选择js    
                                
|     |---- README.MD  # 安装使用方法                    
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/hihopeorg/wx_calendar/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/hihopeorg/wx_calendar/pulls) 。

## 开源协议
本项目基于 [Apache License 2.0](https://gitee.com/hihopeorg/wx_calendar/blob/master/LICENSE) ，请自由地享受和参与开源。

