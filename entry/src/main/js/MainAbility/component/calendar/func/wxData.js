class WxData {
  constructor(component) {
    this.Component = component
  }
  getData(key) {
    const data = this.Component

    if (!key) return data
    if (key.includes('.')) {
      let keys = key.split('.')
      const tmp = keys.reduce((prev, next) => {
        return prev[next]
      }, data)
      return tmp
    } else {
//      console.log(JSON.stringify(this.Component.data[key]))
      return this.Component[key]
    }
  }
  setData(data) {
    if (!data) return
    if (typeof data === 'object') {
      for(var key in data){
        if (key.indexOf(".")>0) {
          let list=key.split(".")

          this.Component[list[0]][list[1]]=data[key];
        }else{
          this.Component[key]=data[key];
        }
      }
    }
  }
}
export default WxData
