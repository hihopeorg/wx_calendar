import Week from './func/week'
import { Logger, Slide, GetDate} from './func/utils'
import initCalendar, {
  jump,
  getCurrentYM,
  whenChangeDate,
  renderCalendar,
  whenMulitSelect,
  whenSingleSelect,
  whenChooseArea,
  getCalendarDates,
  getSelectedDay,
  cancelSelectedDates,
  setTodoLabels,
  deleteTodoLabels,
  getTodoLabels,
  clearTodoLabels,
  disableDay,
  enableArea,
  enableDays,
  switchView,
  setCalendarConfig,
  getCalendarConfig,
  setSelectedDays
} from './main.js'

const slide = new Slide()
const logger = new Logger()
const getDate = new GetDate()

export default{
  options: {
    styleIsolation: 'apply-shared',
    multipleSlots: true // 在组件定义时的选项中启用多slot支持
  },
  props: {
    config: {default:{}},
    calendarConfig:{default:{}},
    // message 的 icon 图标样式
  },
  data: {
    handleMap: {
      prev_year: 'chooseYear',
      prev_month: 'chooseMonth',
      next_month: 'chooseMonth',
      next_year: 'chooseYear'
    },
    calendar:{curYear:new Date().getFullYear(),curMonth:new Date().getMonth()+1,days:{}},
    gesture:{},
  },
  onInit(){
      this.initComp()
      this.enableAreaPro = this.calendar.enableAreaPro;
  },
  initComp() {
    const calendarConfig = this.setDefaultDisableDate()
    this.setConfig(calendarConfig)
  },
  setDefaultDisableDate() {
    const calendarConfig = this.calendarConfig || {}
    if (calendarConfig.disableMode && !calendarConfig.disableMode.date) {
      calendarConfig.disableMode.date = getDate.toTimeStr(getDate.todayDate())
    }
    return calendarConfig
  },
  setConfig(config) {
    if (config.markToday && typeof config.mar0kToday === 'string') {
      config.highlightToday = true
    }
    config.theme = config.theme || 'default'
    this.weekMode = config.weekMode;
    this.calendarConfig=config;

    initCalendar(this, config);
    this.calendar = {...this.calendar,jump,getSelectedDay,cancelSelectedDates,setTodoLabels,getCurrentYM,deleteTodoLabels,getTodoLabels,clearTodoLabels,disableDay,enableArea,enableDays,switchView,setCalendarConfig,getCalendarConfig,getCalendarDates,setSelectedDays}
    this.calendar.days = getCalendarDates()
    for(let i =0;i<this.calendar.days.length;i++){
      this.calendar.days[i] = {...this.calendar.days[i],disable:false}
    }

    //初始化是设置所有日期不选中状态
    //this.calendar.days = this.calendar.days.filter((item)=>{
    //  item.choosed = false;
    //  return item
    //});
  },
  chooseDate(e) {
    const  type  = e;
    if (!type) return
    const methodName = this.handleMap[type];

    this[methodName](type);
  },
  chooseYear(type) {
    const { curYear, curMonth } = this.calendar
    if (!curYear || !curMonth) return logger.warn('异常：未获取到当前年月')
    if (this.weekMode) {
      return console.warn('周视图下不支持点击切换年月')
    }
    let newYear = +curYear
    let newMonth = +curMonth
    if (type === 'prev_year') {
      newYear -= 1
    } else if (type === 'next_year') {
      newYear += 1
    }
    this.render(curYear, curMonth, newYear, newMonth)
  },
  chooseMonth(type) {
    const { curYear, curMonth } = this.calendar
    if (!curYear || !curMonth) return logger.warn('异常：未获取到当前年月')
    if (this.weekMode) return console.warn('周视图下不支持点击切换年月')
    let newYear = +curYear
    let newMonth = +curMonth
    if (type === 'prev_month') {
      newMonth = newMonth - 1
      if (newMonth < 1) {
        newYear -= 1
        newMonth = 12
      }
    } else if (type === 'next_month') {

      newMonth += 1
      if (newMonth > 12) {
        newYear += 1
        newMonth = 1
      }
    }
    this.render(curYear, curMonth, newYear, newMonth)
  },
  render(curYear, curMonth, newYear, newMonth) {
    whenChangeDate.call(this, {
      curYear,
      curMonth,
      newYear,
      newMonth
    })
    this.calendar.curYear = newYear;
    this.calendar.curMonth = newMonth;
    console.info("this.calendar.curYear ======================" + this.calendar.curYear)
    console.info("this.calendar.curMonth ======================" + this.calendar.curMonth)

    renderCalendar.call(this, newYear, newMonth);
  },
  /**
   * 日期点击事件
   * @param {!object} e 事件对象
   */
  tapDayItem(index,item) {
    const  idx = index;
    const { day, disable } = item
    if (disable || !day) return
    const config = this.calendarConfig || this.config || {}
    const { multi, chooseAreaMode } = config

    if (multi) {
      whenMulitSelect.call(this, idx)
    } else if (chooseAreaMode) {
      whenChooseArea.call(this, idx)
    } else {
      whenSingleSelect.call(this, idx);
    }
    this.calendar.noDefault = false;
//    this.setData({
//      'calendar.noDefault': false
//    })
  },
  doubleClickToToday() {
    if (this.config.multi || this.weekMode) return
    if (this.count === undefined) {
      this.count = 1
    } else {
      this.count += 1
    }
    if (this.lastClick) {
      const difference = new Date().getTime() - this.lastClick
      if (difference < 500 && this.count >= 2) {
        jump.call(this)
      }
      this.count = undefined
      this.lastClick = undefined
    } else {
      this.lastClick = new Date().getTime()
    }
  },
  /**
   * 日历滑动开始
   * @param {object} e
   */
  calendarTouchstart(e) {
    const t = e.touches[0]
    const startX = t.globalX
    const startY = t.globalY
    this.slideLock = true // 滑动事件加锁
    this.gesture.startX = startX
    this.gesture.startY = startY
//    this.setData({
//      'gesture.startX': startX,
//      'gesture.startY': startY
//    })
  },
  /**
   * 日历滑动中
   * @param {object} e
   */
  calendarTouchmove(e) {
    const { gesture } = this
    const { preventSwipe } = this.calendarConfig
    if (!this.slideLock || preventSwipe) return
    if (slide.isLeft(gesture, e.touches[0])) {
      this.handleSwipe('left')
      this.slideLock = false
    }
    if (slide.isRight(gesture, e.touches[0])) {
      this.handleSwipe('right')
      this.slideLock = false
    }
  },
  calendarTouchend() {
    this.calendar.leftSwipe = 0;
    this.calendar.rightSwipe = 0;
//    this.setData({
//      'calendar.leftSwipe': 0,
//      'calendar.rightSwipe': 0
//    })
  },
  handleSwipe(direction) {
//    let swipeKey = 'calendar.leftSwipe'
    let swipeCalendarType = 'next_month'
    let weekChangeType = 'next_week'
    if (direction === 'right') {
//      swipeKey = 'calendar.rightSwipe'
      swipeCalendarType = 'prev_month'
      weekChangeType = 'prev_week'
    }
//    this.[swipeKey]= 1;
    this.currentYM = getCurrentYM()
    if (this.weekMode) {
      this.slideLock = false
      this.currentDates = getCalendarDates()
      if (weekChangeType === 'prev_week') {
        Week(this).calculatePrevWeekDays()
      } else if (weekChangeType === 'next_week') {
        Week(this).calculateNextWeekDays()
      }
      this.onSwipeCalendar(weekChangeType)
      this.onWeekChange(weekChangeType)
      return
    }
    this.chooseMonth(swipeCalendarType)
    this.onSwipeCalendar(swipeCalendarType)
  },
  onSwipeCalendar(direction) {
    this.$emit('onSwipe', {
      directionType: direction,
      currentYM: this.currentYM
    })
  },
  onWeekChange(direction) {
    this.$emit('whenChangeWeek', {
      current: {
        currentYM: this.currentYM,
        dates: [...this.currentDates]
      },
      next: {
        currentYM: getCurrentYM(),
        dates: getCalendarDates()
      },
      directionType: direction
    })
    this.currentDates = null
    this.currentYM = null
  },
  //供demo使用
  getCalendar(){
    return this.calendar;
  },
}
