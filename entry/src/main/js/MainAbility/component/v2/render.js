import plugins from './plugins/index'
import { getCalendarConfig } from './utils/index'
/**
 * 渲染日历
 */
export function renderCalendar(calendarData, calendarConfig,flag) {
  //这里  disableDates   属性不能添加到calendarData对象中去，第一次点击没问题 后面再点击会报错  不知道为什么  所以重写了这个方法
  // 这个属性位置只能在这 不然不生效
  if(calendarData) {
    let renderCausedBy = calendarData.renderCausedBy;
    if ("disableDates" == renderCausedBy) {
      let disableList = calendarData.disableDatesPre;
      let dates = calendarData.dates;
      let datelist = disableList[0].split("-");
      calendarData.dates = dates.filter((item) => {
        if (item.year == datelist[0] && item.month == datelist[1] && item.date == datelist[2]) {
          item.disable = true;
        }
        return item
      });
    }
  }
  let _this = this;
  return new Promise(resolve => {
    const Component = this
    if (_this.firstRender === void 0) {
      _this.firstRender = true
    } else {
      _this.firstRender = false
    }
    const exitData = _this.calendar || {}
    if(!flag){
      for (let plugin of plugins.installed) {
        const [, p] = plugin
        if (typeof p.beforeRender === 'function') {
          const {
            calendarData: newData,
            calendarConfig: newConfig
          } = p.beforeRender(
            { ...exitData, ...calendarData },
            calendarConfig || getCalendarConfig(Component),
            Component
          )
          calendarData = newData;
          calendarConfig = newConfig;
        }
      }
    }
    //这里  enableArea   属性不能添加到calendarData对象中去，第一次点击没问题 后面再点击会报错  不知道为什么  所以重写了这个方法
    //enableDates也是一样
    // 这两个属性位置只能在这 不然不生效
    if(calendarData){
      let renderCausedBy=calendarData.renderCausedBy;
      if("enableArea" == renderCausedBy){
        let enableAreaList = calendarData.enableAreaPre;
        let dataStart =enableAreaList[0].split("-");
        let dataEnd =enableAreaList[1].split("-");
        let dates=calendarData.dates;
        calendarData.dates = dates.filter((item)=>{
          item.disable = false;
          return item
        });
        calendarData.dates = dates.filter((item)=>{
          if(item.year == dataStart[0] && item.month == dataStart[1] && item.date < dataStart[2]){
            item.disable = true;
          }else if(item.year == dataEnd[0] && item.month == dataEnd[1] && item.date > dataEnd[2]){
            item.disable = true;
          }else if(item.year == dataStart[0] && item.month == dataStart[1]){
            item.disable = false;
          }
          return item;
        });
      }else if("enableDates" == renderCausedBy){
        let enableDateslist = calendarData.enableDatesPre;
        let dates=calendarData.dates;
        let datelist
        for(let i=0;i<dates.length;i++){
          if(!dates[i].showFlagSign){
            dates[i].disable=true;
          }
          for(let j=0;j<enableDateslist.length;j++){
            datelist = enableDateslist[j].split("-");
            if(dates[i].year == datelist[0] && dates[i].month == datelist[1] && dates[i].date == datelist[2]){
              dates[i].disable=false;
              dates[i].showFlagSign=true;
              break;
            }
          }
        }
      }
    }
    Component.config = calendarConfig;
    Component.calendar = calendarData;
    const rst = {
      calendar: calendarData,
      config: calendarConfig,
      firstRender: _this.firstRender
    }
    console.info("aaaaaa===================" + JSON.stringify(calendarData))
    resolve(rst)
    if (_this.firstRender) {
      _this.$emit("afterCalendarRender",rst);
      _this.firstRender = false;
    }
  })
}
