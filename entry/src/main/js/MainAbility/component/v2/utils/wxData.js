class WxData {
  constructor(component) {
    //console.info("wxData.js/constructor/component================" + JSON.stringify(component))

    this.Component = component
  }
  getData(key) {
    const a = this.Component
    const data = a
    if (!key) return data
    if (key.includes('.')) {
      let keys = key.split('.')
      const tmp = keys.reduce((prev, next) => {
        return prev[next]
      }, data)
      return tmp
    } else {
      return this.Component[key]
    }
  }
  setData(data) {
    return new Promise((resolve, reject) => {
      if (!data) return reject('no data to set')
      if (typeof data === 'object') {
        this.Component.setData(data, () => {
          resolve(data)
        })
      }
    })
  }
}

export default WxData
