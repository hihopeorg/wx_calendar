let chooseYear = null
let chooseMonth = null
export default{
  data: {
    hasEmptyGrid: false,
    showPicker: false,
    multitextvalue:'',
    multitext:[[], []],
    multitextselect:[1,2,0],
    curYear:'',
    curMonth:'',
    weeksCh:'',
    days:'',
  },
  onInit() {
    this.multitext[0];
    let currentYear = new Date().getFullYear();

    for(let i=0;i<100;i++){
      this.multitext[0].push(parseInt(currentYear+i)+"年")
    }
    for(let i=1;i<13;i++){
      this.multitext[1].push(parseInt(i)+"月")
    }
    const date = new Date()
    const curYear = date.getFullYear()
    const curMonth = date.getMonth() + 1
    const weeksCh = ['日', '一', '二', '三', '四', '五', '六']
    this.calculateEmptyGrids(curYear, curMonth)
    this.calculateDays(curYear, curMonth)
    this.curYear= curYear
    this.curMonth= curMonth
    this.weeksCh= weeksCh
  },
  getThisMonthDays(year, month) {
    return new Date(year, month, 0).getDate()
  },
  getFirstDayOfWeek(year, month) {
    return new Date(Date.UTC(year, month - 1, 1)).getDay()
  },
  calculateEmptyGrids(year, month) {
    const firstDayOfWeek = this.getFirstDayOfWeek(year, month)
    let empytGrids = []
    if (firstDayOfWeek > 0) {
      for (let i = 0; i < firstDayOfWeek; i++) {
        empytGrids.push(i)
      }
      this.hasEmptyGrid = true;
      this.empytGrids = empytGrids;
    } else {
      this.hasEmptyGrid = false;
      this.empytGrids = [];
    }
  },
  calculateDays(year, month) {
    let days = []

    const thisMonthDays = this.getThisMonthDays(year, month)

    for (let i = 1; i <= thisMonthDays; i++) {
      days.push({
        day: i,
        choosed: false
      })
    }
    this.days = days;
  },
  handleCalendar(e) {
    const handle = e
    const curYear = this.curYear
    const curMonth = this.curMonth
    if (handle === 'prev') {
      let newMonth = parseInt(curMonth) - 1
      let newYear = curYear
      if (newMonth < 1) {
        newYear = parseInt(curYear) - 1
        newMonth = 12
      }
      this.calculateDays(newYear, newMonth)
      this.calculateEmptyGrids(newYear, newMonth)
      this.curYear = newYear;
      this.curMonth = newMonth;
    } else {
      let newMonth = curMonth + 1
      let newYear = curYear
      if (newMonth > 12) {
        newYear = curYear + 1
        newMonth = 1
      }
      this.calculateDays(newYear, newMonth)
      this.calculateEmptyGrids(newYear, newMonth)
      this.curYear = newYear;
      this.curMonth = newMonth;
    }
  },
  tapDayItem(e) {
    const idx = e.currentTarget.dataset.idx
    const days = this.data.days
    days[idx].choosed = !days[idx].choosed
    this.setData({
      days
    })
  },
  chooseYearAndMonth() {
    const curYear = this.data.curYear
    const curMonth = this.data.curMonth
    let pickerYear = []
    let pickerMonth = []
    for (let i = 1900; i <= 2100; i++) {
      pickerYear.push(i)
    }
    for (let i = 1; i <= 12; i++) {
      pickerMonth.push(i)
    }
    const idxYear = pickerYear.indexOf(curYear)
    const idxMonth = pickerMonth.indexOf(curMonth)
    this.pickerValue = [idxYear, idxMonth];
    this.pickerYear = pickerYear;
    this.pickerMonth = pickerMonth;
    this.showPicker = true;
    this.$element("zenmehuishi").show();
  },
  pickerChange(e) {
    const val = e.newValue
    chooseYear = parseInt(val[0])
    chooseMonth = parseInt(val[1])
    this.calculateEmptyGrids(chooseYear, chooseMonth);
    this.calculateDays(chooseYear, chooseMonth);
    this.curYear = chooseYear;
    this.curMonth = chooseMonth;

  },
}
