// @ts-nocheck
import todo from '../../component/v2/plugins/todo'
import selectable from '../../component/v2/plugins/selectable'
import solarLunar from '../../component/v2/plugins/solarLunar/index'
import timeRange from '../../component/v2/plugins/time-range'
import week from '../../component/v2/plugins/week'
import holidays from '../../component/v2/plugins/holidays/index'
import plugin from '../../component/v2/plugins/index'
import jump from '../../component/v2/plugins/preset/base'

plugin
    .use(todo)
    .use(solarLunar)
    .use(selectable)
    .use(week)
    .use(timeRange)
    .use(holidays)
import prompt from '@system.prompt';
export default{
    data: {
        calendarConfig: {
            theme: 'elegant',
            // showHolidays: true,
            // emphasisWeek: true,
            // chooseAreaMode: true
            // defaultDate: '2020-9-8',
      // autoChoosedWhenJump: true
        },
        actionBtn: [
            {
                text: '跳转指定日期',
                action: 'jump',
                color: 'olive'
            },
            {
                text: '获取当前已选',
                action: 'getSelectedDates',
                color: 'red'
            },
            {
                text: '取消选中日期',
                action: 'cancelSelectedDates',
                color: 'mauve'
            },
            {
                text: '设置待办事项',
                action: 'setTodos',
                color: 'cyan'
            },
            {
                text: '删除指定代办',
                action: 'deleteTodos',
                color: 'pink'
            },
            {
                text: '清空待办事项',
                action: 'clearTodos',
                color: 'red'
            },
            {
                text: '获取所有代办',
                action: 'getTodos',
                color: 'purple'
            },
            {
                text: '禁选指定日期',
                action: 'disableDates',
                color: 'olive'
            },
            {
                text: '指定可选区域',
                action: 'enableArea',
                color: 'pink'
            },
            {
                text: '指定特定可选',
                action: 'enableDates',
                color: 'red'
            },
            {
                text: '选中指定日期',
                action: 'setSelectedDates',
                color: 'cyan'
            },
            {
                text: '周月视图切换',
                action: 'switchView',
                color: 'orange'
            },
            {
                text: '获取自定义配置',
                action: 'getConfig',
                color: 'olive'
            },
            {
                text: '获取日历面板日期',
                action: 'getCalendarDates',
                color: 'purple'
            }
        ],
        rst:[],
        rstStr:'',
    },
    afterTapDate(e) {
        console.log('afterTapDate', e)
    },
    whenChangeMonth(e) {
        console.log('calendarV2/whenChangeMonth', e)
        console.log('whenChangeMonth', e)
    },
    whenChangeWeek(e) {
        console.log('whenChangeWeek', e)
    },
    takeoverTap(e) {
//        console.log('takeoverTap', e.detail)
        console.log(JSON.stringify( e))
    },
    afterCalendarRender(e) {
        console.log('afterCalendarRender', e)
    },
    onSwipe(e) {
        console.log('onSwipe', e)
    },
    showToast(msg) {
        if (!msg || typeof msg !== 'string') return
        prompt.showToast({
            message: msg,
            duration: 2000
        })
    },
    generateRandomDate(type) {
        let random = ~~(Math.random() * 10)
        switch (type) {
            case 'year':
                random = 201 * 10 + ~~(Math.random() * 10)
                break
            case 'month':
                random = (~~(Math.random() * 10) % 9) + 1
                break
            case 'date':
                random = (~~(Math.random() * 100) % 27) + 1
                break
            default:
                break
        }
        return random
    },
    handleAction(e) {
        const  action  = e;
        this.rst = [];
        const calendar = this.$child("calendar").getCalendar();

        //const { year, month } = calendar.getCurrentYM();
        const  year = calendar.curYear;
        const month  = calendar.curMonth;

        let arr = [
            year+"-"+month+"-"+this.generateRandomDate('date'),
            year+"-"+month+"-"+this.generateRandomDate('date'),
            year+"-"+month+"-"+this.generateRandomDate('date'),
            year+"-"+month+"-"+this.generateRandomDate('date'),
            year+"-"+month+"-"+this.generateRandomDate('date'),
        ]
        switch (action) {
            case 'config':
                console.info("liqiliqi")
                calendar
                    .setCalendarConfig({
                        showLunar: false,
                        theme: 'elegant',
                        multi: true
                    })
                    .then(conf => {
                        console.log('设置成功：', conf)
                    })
                break
            case 'getConfig':
                this.showToast('请在控制台查看结果')
                console.log('自定义配置: ')
                console.log(JSON.stringify(calendar.getCalendarConfig()))
                break
            case 'jump': {
                const year = this.generateRandomDate('year')
                const month = this.generateRandomDate('month')
                const date = this.generateRandomDate('date')
               //li const configed = calendar.calendarConfig()
                const configed = this.calendarConfig
                if (configed.weekMode) {
                    calendar['weekModeJump']({ year, month, date })
                } else {
                    //jump({ year, month, date })
                    calendar[action]({ year, month, date })
                }
                break
            }
            case 'getSelectedDates': {
                const selected = calendar.getSelectedDates();
                if (selected == "")return this.showToast('当前未选择任何日期');
                this.showToast('请在控制台查看结果');
                this.rst = [JSON.stringify(selected)];
                console.log('get selected dates ='+ this.rst);
                break
            }
            case 'cancelSelectedDates':
                calendar[action](calendar.getSelectedDates())
                break
            case 'setTodos': {
                const dates = [
                    {
                        year,
                        month,
                        date: this.generateRandomDate('date'),
                        todoText: Math.random() * 10 > 5 ? '领奖日' : ''
                    }
                ]
                calendar[action]({
                    showLabelAlways: true,
                    dates
                })
                console.log('set todo = ' + JSON.stringify(dates))
                break
            }
            case 'deleteTodos': {
                const todos = [...calendar.getTodos()]
                if (todos.length) {
                    calendar[action]([todos[0]]).then(() => {
                        const _todos = [...calendar.getTodos()]
                        setTimeout(() => {
                            const rst = _todos.map(item => JSON.stringify(item))
                            this.rst = rst;
                            console.log('delete todo =' + JSON.stringify(todos[0]));
                        })
                    })
                } else {
                    this.showToast('没有待办事项')
                }
                break
            }
            case 'clearTodos':
                if (![...calendar.getTodos()] || ![...calendar.getTodos()].length) {
                    return this.showToast('没有待办事项')
                }
                calendar[action]()
                break
            case 'getTodos': {
                const selected = calendar[action]()
                if (!selected || !selected.length)
                return this.showToast('未设置待办事项')
                const rst = selected.map(item => JSON.stringify(item))
                rst.map(item => JSON.stringify(item))
                this.rst = rst
                break
            }
            case 'disableDates':
                calendar[action]([
                    {
                        year:year,
                        month:month,
                        date: this.generateRandomDate('date')
                    }
                ])
                break
            case 'enableArea': {
                let sdate = this.generateRandomDate('date')
                let edate = this.generateRandomDate('date')
                if (sdate > edate) {
                    [edate, sdate] = [sdate, edate]
                }
                const area = [`${year}-${month}-${sdate}`, `${year}-${month}-${edate}`]
                calendar[action](area)
                this.rstStr=JSON.stringify(area);
                break
            }
            case 'enableDates':
                calendar[action](arr)
                this.rstStr = JSON.stringify(arr);
                break
            case 'switchView':
                console.log(this.week)
                if (!this.week) {
                    calendar[action]('week').then(calendarData => {
                        console.log('switch success!')
                        console.log(JSON.stringify(calendarData))
                    })
                    this.week = true
                } else {
                    calendar[action]().then(calendarData => {
                        console.log('switch success!')
                        console.log(JSON.stringify(calendarData))
                    })
                    this.week = false
                }
                break
            case 'setSelectedDates':
                calendar[action]([
                    {
                        year,
                        month,
                        date: this.generateRandomDate('date')
                    },
                    {
                        year,
                        month,
                        date: this.generateRandomDate('date')
                    }
                ])
                break
            case 'getCalendarDates':
                this.showToast('请在控制台查看结果');
                console.log(JSON.stringify(calendar.getCalendarDates({lunar: true})))
                break
            default:
                break
        }
    }
}


