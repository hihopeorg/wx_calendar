export default{
  data:{
    showDatePicker:false,
    inputTimer:""
  },
  /**
   * 当输入日期时
   * @param {object} e  事件对象
   */
  onInputDate(e) {
    this.inputTimer && clearTimeout(this.inputTimer)
    this.inputTimer = setTimeout(() => {
      const v = e.value
      const _v = (v && v.split('-')) || []
      const RegExpYear = /^\d{4}$/
      const RegExpMonth = /^(([0]?[1-9])|([1][0-2]))$/
      const RegExpDay = /^(([0]?[1-9])|([1-2][0-9])|(3[0-1]))$/
      if (_v && _v.length === 3) {
        if (
        RegExpYear.test(_v[0]) &&
        RegExpMonth.test(_v[1]) &&
        RegExpDay.test(_v[2])
        ) {
          this.$child("calendar").jump(+_v[0], +_v[1], +_v[2])
        }
      }
    }, 500)
  },
  callDatepicker(){
    this.showDatePicker = true;
  },
  closeDatePicker(){
    //this.showDatePicker = false;
  }
}
